# Personal Website Template
![Personal Website - Lite Mode](https://i.imgur.com/3lbHQrh.png)
![Personal Website - Dark Mode](https://i.imgur.com/bUIAalX.png)

<p align="center"><sub>Code with ♥️</sub></p>

---
<p  align="center">
    
<a>[![Donate](https://img.shields.io/badge/$-donate-3366FF.svg)](https://www.ko-fi.com/hraverkar) </a>

</p>

# Features ✨

Automatically shows your GitHub public repo, blog posts (dev.to or medium) on your personal website.

-   Show Github Repo

-   Show Medium or Dev.to Blog Posts

# Usage

1. Clone the Repository

    ```
     git clone https://github.com/hraverkar/hraverkar.git
    ```

2. Run this command to install dependencies

    ```
     npm install
    ```

3. Open src/config.js and then enter your social media account username.

4. Customize about us page from src/components/About/about.js

5. Run

    ```
    npm run start
    ```


# 🤝 Contributing

Contributions, issues and feature requests are welcome!

# Show your support

Give a ⭐️ if this project helped you!

# Fork from 

Website fork from [Pjijin](https://github.com/PJijin)
